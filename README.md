# site repo for www.kurousada.ga

[![Netlify Status](https://api.netlify.com/api/v1/badges/599d822d-ad6c-4e09-8070-71b4769f2473/deploy-status)](https://app.netlify.com/sites/kurousada/deploys)

Visit: [www.kurousada.ga](https://www.kurousada.ga/).

## Install

As my personal memo:

```
$ git clone https://gitlab.com/kurousada/site.git
$ cd site
$ hugo server
```

## Set up env vars

See netlify doc: [Build Environment Variables](https://www.netlify.com/docs/continuous-deployment/#build-environment-variables) and set following env vars.

 - HUGO_GOOGLE_ANALYTICS
