---
templateKey: about-page
title: About
locale: ja-JP
date: 2019-03-02T20:05:00.000Z
description: このサイトがどうやってできているのかというポエム
draft: false
tags:
  - 'lang:ja'
---
おはこんばんちは。
このサイトを作った **Kuro Usada** と申します。

* Twitter: [@kurousada](https://twitter.com/kurousada)
* GitLab: [kurousada](https://gitlab.com/kurousada)
* Linktree: [kurousada](https://linktr.ee/kurousada)

_好きなものはディズニー･プログラミング･食べることです！_

なんて調子の自己紹介を長々書いても誰も読まないと思うので、あなたがこのサイトを見ている裏で何が起きているか、というポエムを代わりに書きます。

このサイトのrepoは[gitlab.com/kurousada/site](https://gitlab.com/kurousada/site)です。

昔はみんな、そのサイトを作成した環境情報としてOSとかCPUとかがトップページやAboutページに書いてあったよねぇ……
あぁ、何もかもが懐かしい……

<div class="note">
結局、GatsbyからHugoへ戻しました。
また、Netlify CMSではなくGitLab Web IDEを使うようになり、それに伴ってプルリクベース運用もやめました
（2020/04/29）
</div>

## 全体像

![サイトの構成図](/img/architecture.png "サイトの構成図")

構成はHugo + GitLab + Netlifyです。

記事の追加や編集はGitLab Web IDE。

ドメインはFreenom、https化はNetlifyがLet's Encryptを使って上手くやってくれます。

## 記事を作成する時のフロー

個別の記事はmarkdownで書き、Hugoという静的サイトジェネレータを使って変換し、レイアウトに埋め込んでいます。
記事一覧などもこの時に生成します。

このように内容と表現の分離を行うことで管理しやすくなり、新しい記事を書く時にも内容やタイトルに集中できるようになっています（集中してるとは言ってない）。

1. GitLab Web IDEで記事を作成
2. コミットすると勝手にgit pushしてビルドとデプロイしてくれる

Netlify CMSで編集しないのは、Netlify CMSが自動保存ではないため原稿書いてて何かの拍子に消えると悲しいからです。

masterにpushするとあとはNetlifyが自動でビルドとデプロイしてくれます。

## 技術やサービスの選定基準

元々はHugo + GitHub + GitHub pagesという構成だったんですが、Netlifyが便利すぎてGatsbyに乗り換えました。
……と思ったら、結局Hugoに戻りました。

### Netlify / Netlify CMS

もうね、すべてこいつのおかげです。
素晴らしいです。

……と書いていましたが、結局Netlify CMSよりGitLab Web IDEが便利なのでNetlifyはCIかつCDNとして使っています。

### Hugo

Hugoは自分が好きなGoで書かれてるし、速度的には素晴らしいんですが、如何せんテンプレートが弄りにくくて少し不便を感じていました。

Shortcodesとか ~~キモイ~~ ちょっとなぁ、って感じですし。
Themeは使いやすいけど。

というわけでHugoが殊更に悪いわけじゃないんですが、なんか痒いところに手が届かないことが重なったため、Netlifyに移行するタイミングでgatsbyに乗り換えました。

が、その後に結局Hugoに戻ってきました。
Reactが自分の用途だと正直大げさだったのと、いいテーマが見つかったこと、あとはshortcodesはwebcomponentsを書けば代替可能だと思ったからです。

HugoがAsciidocを（ちゃんとした速度で）サポートしてくれたのも大きいですが、上述の通りMarkdown+webcomponentsがいいかなと思っています。

### GitLab

前はGitHubにrepo置いてGitHub pagesでホスティングしてました。

が、MSに買収されたことと、GitLabがOSSであること、それによって無料でローカルにもインストール出来ることなどを考慮し、GitLabに移りました。

とはいえサイト以外ではGitHubも使って（？）ますが……

ここもGitLab使ったことないから使ってみようかなって動機が大きいですね。

~~でもGitLabのが使いやすく思えてきたんで完全に移行してもいいかもなあ、なんて。~~

今では完全にGitLabに依存しています。
GitHubより機能が豊富で使いやすいんですよね。
特にGitLab Web IDEが出てきて編集も簡単になったのが大きいです。

### Markdown

これに関してはもう諦めですね 笑

というのも元々markdownには機能不足を感じていまして、例えば表のalignmentを制御出来ないとか、noteやinfoといったパーツがないとか、ToC（目次）を挿入できないとか……

で、以前からAsciidocを使おうと思ってたんですが、なんとHugoでAsciidocを変換しようとすると、ruby製のasciidoctorか、そのJS portであるasciidoctor.jsを使うため、とてつもなく遅くなるんですよ！

これじゃあ意味ないやん……って思ってHugoのgithub issues見てたら｢asciidoctorをGoにportしよう｣ってなってたんです。
わーい！と一瞬歓喜したものの、よく読むとそれが全然進んでないんですよ。

<div class="note">
今は実装も進み、ちゃんと実用的な速度になってます。
</div>

で、Netlifyに乗り換えるタイミングだったこともありgatsbyに乗り換えたんですが、案の定asciidoctor.jsは遅い。

その上、Asciidocの多種多様な機能に対応したcssを書くのはたるい。

テーマ使えって話ですが、そもそもAsciidocターゲットのテーマが少ない上にテーマを使っても結局カスタマイズするんだから、それなら最初から自分で書くわ！！ってなるんで。

というわけでとりあえずmarkdownで我慢して書きつつ、足りないところはHTML直書きすっか……となりました。

これなら将来asciidoctorが早くなった時にもpandasなりなんなりで原稿変換できるし、最悪Web Componentsが来れば足りない機能は自分でコンポーネント書きまくればいい話でしょ？

とかいってたらだんだんwebcomponentsが使えるようになってきて、自分の中にもある程度知見が溜まってきたのでこの方向で行こうと思っています。

### Freenom

ドメインについて書くの忘れてた！

Freenomという無料でドメインを取得できるサービスを使いました。
`.ga`は[ガボン共和国](https://ja.wikipedia.org/wiki/%E3%82%AC%E3%83%9C%E3%83%B3)のccTLDです。

期限切れ前の1ヶ月間で更新すれば永年無料のようです。
すごいや！

## 出てきたやつら一覧

 - [Hugo](https://gohugo.io/) - A Static Site Generator
 - [Noteworthy](https://github.com/kimcc/hugo-theme-noteworthy) - A Hugo Theme
 - [GitLab](https://gitlab.com/) - A Git Hosting Service
 - [Netlify](https://app.netlify.com/) - A Headless CMS Service
 - [Freenom](https://freenom.com/) - Free Domains

## おわりに

ここまで読むなんてありがとうございます。
余程暇なんですね。

暇ついでに他の記事も読んでってくださいよ？
お願いしましたからね？ね？
