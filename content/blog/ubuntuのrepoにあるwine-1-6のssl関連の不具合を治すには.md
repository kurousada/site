---
templateKey: blog-post
title: UbuntuのrepoにあるWine 1.6のSSL関連の不具合を治すには
locale: ja-JP
date: 2017-10-20T02:33:00.000Z
description: How to fix Wine's SSL problems by installing latest one
draft: false
tags:
  - linux
  - tech
  - 'lang:ja'
---
<!--more-->

<p class="note">かなり昔（Ubuntu 16.4の頃）に書いた草稿ですが、何かの役に立つかもしれないので今さら公開しておきます（2020/01/08）。なお、<a href="https://wiki.winehq.org/Ubuntu">Wineの公式Wikiにある手順</a>に従うのが一番よいです。</p>

 - wine-1.6はSSL未サポート→証明書をverifyできず、「unauthorized certification error」になる（わかりにくい）
 - wineの最新版を入れるには[https://wiki.winehq.org/Ubuntu](https://wiki.winehq.org/Ubuntu)
 - なお、旧来のlounchpadのPPAは2017/03/28にdeprecated→[https://launchpad.net/~wine/+archive/ubuntu/wine-builds](https://launchpad.net/~wine/+archive/ubuntu/wine-builds)
 - 経緯→[https://www.winehq.org/pipermail/wine-devel/2017-March/117104.html](https://www.winehq.org/pipermail/wine-devel/2017-March/117104.html)

インストールする時は wine-mono, wine-gecko, winetricksなどをapt removeしてから（古いwineは自動で消してくれるようだ）

```
$ sudo apt remove wine wine-mono wine-gecko winetricks
```

64bit環境ならi386を追加

```
$ sudo dpkg --add-architecture i386
```

[https://wiki.winehq.org/Ubuntu](https://wiki.winehq.org/Ubuntu) のとおり

```
$ sudo apt install --install-recommends winehq-stable
```

```
$ sudo apt install winetricks
$ winetricks
```

で、wine-monoとwine-geckoをインストールするか聞かれるので、「インストール」を選択（代わりにシステムのパッケージ使えるとか出てくるけど、WineHQのwikiに従ってやめておく）

最後にwgetしたRelease.keyを消して終了

```
$ rm Release.key
```
