---
templateKey: blog-post
title: The fastest way to Tokyo DisneySea from Tokyo Station by train
locale: en-US
date: 2019-02-17T03:07:11.423Z
description: >-
  Do you clearly understand the way to Tokyo DisneySea from Tokyo station?

  Do you know the fastest way using trains?

  Check it out with photos and explanations by Japanese Disney mania, Kuro
  Usada!
draft: false
tags:
  - 'lang:en'
  - disney
  - tds
---
Do you clearly understand the way to Tokyo DisneySea from Tokyo station?<br>Do you know the fastest way using trains?<br>Check it out with photos and explanations by Japanese Disney mania, Kuro Usada!

<!--more-->

OK, you got long flight and finally get in Japan, Tokyo station.

It's early morning and suddenly you hear the mysterious voice: _"Go to Tokyo DisneySea and experince Sindbad Storybook Voyage by half an hour, or you will be in trouble..."_

It's not a big matter for those who have his/her house in the Toontown, but unfortunately you aren't and must foresee yourself in trouble.

Don't panic nor give up all hope. Just continue to read this post!

...before that, ensure you have tickets or IC cards (such as Suica or PASMO) to take trains to **JR Maihama** station from where you are.

## First step: Tokyo to Maihama

You need to get on **JR Keiyo Line** from Tokyo to go to JR Maihama station which is near-by Tokyo Disney Resort.

![Signboard for platforms of Keiyo Line](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150102.jpg)

You can't read Japanese printed on it?
No problem, take it easy.

It's less difficult than you think because JR has theme colors for each line and **Keiyo Line's color is red** as you can see on the signboard.
So follow the color and signboard around you. Really, around you.

It takes long way (**around 10-15 min!**) to actual platform for Keiyo Line from other lines like Shinkan-sen, Yamanote Line, Chuo Line and many other subways. There are lots of escalators and even moving walkways on the path because it's too long to walk. Wow...

Anyway, there are 4 platforms for Keiyo Line and any trains you can get on. **Every trains stop at JR Maihama station**.

<div class="note">
There are <strong>JR Musashino Line</strong> which color is orange on the same platforms. They also stop at Maihama, so take the train you can get on at that time.
</div>

And if you can, get on the 8th car ;)

![The 8th car](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150105.jpg)

It's 15-20 min trip (depending on which train you get, Rapid or Local) and after getting at Maihama, doors on the left side will open.

![Getting off the 8th car](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P_20170615_194540.jpg)

## Step 2: Disney Resort Line

Going down the steps and you'll meet people rushing to the **South Gate**. That's the way you should go to.

After getting out the station, **turn left** and go to the direction of Ikspiari, a shopping center run by Oriental Land co. (which runs Tokyo Disney Resort).

![Getting out the South Gate](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P_20170615_185812_HDR.jpg)
![Move forward to Ikspiari](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150110.jpg)

You will see the entrance of **Disney Resort Gateway station** between Ikspiari's entrance plaza and Tokyo Disney Resort Welcome Center.

![It'a bit tucked away](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150113.jpg)
![Resort Gateway Station](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150114.jpg)

From this station, you get the Disney Resort Line which go around the resort. [See here for fare table](https://www.tokyodisneyresort.jp/en/tdr/resortline/fare).

<div class="note">
One-day/Multi-day pass of Disney Resort Line has <a href="https://www.tokyodisneyresort.jp/en/tdr/resortline/ticket">seasonal designs</a> and good for sourvenir.
</div>

Pass the gate and go up stairs **on your near left side**.

![There might be some seasonal props](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150117.jpg)
![Use the escalator on your near left side](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150120.jpg)

Go forward to end of the platform and get on the **1st car**.

![Getting at platform](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150121.jpg)
![1st car of Disney Resort Linear](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150122.jpg)

## Step 3: Use MiraCosta Gate at DisneySea station

After 10-15 min ride, you will get at Tokyo DisneySea station. It's 2 stops from Resort Gateway station and doors on your left side will open.

On the end of the platform, there is **MiraCosta Gate**.
It is gate only for exit and mainly for guests visiting Hotel MiraCosta (Official Park-side hotel, integrated in Tokyo DisneySea).

By using this gate, you can save a lot of walks and time.

![MiraCosta Gate](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150123.jpg)
![A direction board on the wall](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150127.jpg)

Go down along with the circulating way. There is an elevator also but it's faster to use your foot.

And **DO NOT go to the connecting passage to MiraCosta** in the middle of the way!

![The connecting passage to MiraCosta](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150133.jpg)
![Exit of the stairs](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150134.jpg)
![Exit of the stairs from another point of view](/img/posts/2017/05/the-firstest-way-to-tds-by-train/P6150136.jpg)

Now it's almost there! you will find **North Entrance Gate** of Tokyo DisneySea on your left side.

## Some more tips

There are 2 entrances in Tokyo DisneySea and **they are far apart**. Don't make a mistake when you meet someone there. MiraCosta Gate is near the North Entrance Gate.

Also, MiraCosta Gate is sometimes unavailable (though it's rare).

In such case, get on the 1st car of Disney Resort Line as usual and use the stairs between 1st and 2nd cars at Tokyo DisneySea station.

After getting out the main gate, North Entrance Gate is on your left and vice versa.

Get moved smoothly and save the time in the park!
