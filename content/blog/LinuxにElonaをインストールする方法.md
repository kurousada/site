---
templateKey: blog-post
title: LinuxにElonaをインストールする方法
date: 2021-03-03T00:00:00.000+09:00
description: How to install Elona on Linux
draft: false
locale: ja-JP
tags:
  - lang:ja
  - linux
  - elona
---

ElonaというWindows向けゲームを、Wineを使ってLinuxで動かしましょう。
ヴァリアントももちろん動きます。
これでLinuxでもロミアスを爆殺できますね！

<!--more-->

## Elona?

[Elona](http://ylvania.org/jp/elona) はグラフィックに力を入れた、自由度が大変高いローグライクゲームです。

詳しくは[ニコニコ大百科の記事](https://dic.nicovideo.jp/a/elona)がわかりやすくまとまっているので、そちらを参照されたし。

## 筆者の環境

 - Linux Mint 20.1 Xfce 64bit
 - Wine 6.0
 - Elona 1.22

Debian/Ubuntu以外のディストリビューションでも、Wineがインストールできれば動きます。
Wineのバージョンは3.2以上なら動きます、たぶん（記憶が曖昧）。
Elonaやヴァリアント、Elona Extenderのバージョンはどれでも動くようです。

## Wineをインストール

[Wine HQ](https://wiki.winehq.org/Download)を参考にインストールしましょう。
StableブランチをインストールすればOKです。

## Elonaをインストール

[Ylvania - Elona/Elin Official](http://ylvania.org/jp/elona) から「Elona 1.16 final（安定版）」か「Elona 1.22（開発版）」をダウンロードします。
バージョンはお好みですが、ヴァリアント（素のElonaを元に有志が改造したバージョン）の導入を考えている方はその指示に従いましょう。

<p class="note">
「えー、よくわからないんだけど……」という方は「Elona 1.22（開発版）」をインストールし、後ほど紹介する「Elona Extender」も併せてインストールすることをおすすめします。
</p>

ダウンロードしたファイルをお好きな場所に解凍し、インストール完了です。
unzipコマンドで解凍する際は文字コードをcp932にすること（`unzip -O cp932 elona[バージョン].zip`）をお忘れなく。

## BGMが出るようにする

さて、ここまでくれば `elona.exe` をダブルクリック（または `wine elona.exe`）で遊べます。
しかし、SEは再生されますが、BGMが再生されません。
そこで、[WineにおけるMIDIデバイスの扱い - kakurasanのLinux防備録](https://kakurasan.blogspot.com/2016/01/midi-devices-in-wine.html#h-midi-devices-in-wine-4)を参考にサウンドフォント（gm.dls）とDirect Musicをインストールします。

まず、[winetricks](https://wiki.winehq.org/Winetricks)をインストールします。

```sh
sudo apt install winetricks
```

そして、winetricksを使ってサウンドフォントとDirect Music関連のDLLをインストールします。

```sh
winetricks directmusic gmdls
```

これでBGMを再生できるようになりました。

## フォントを変える

フォントはElonaの設定ファイル（ `config.txt` ）を書き換えれば変えられます。
[Takaoフォント](https://launchpad.net/takao-fonts)や[ふい字](https://hp.vector.co.jp/authors/VA039499/)がおすすめです。

<p class="note">
テキストファイルを開く際はwineに付属のメモ帳（ `notepad` ）を使うと文字化けしません。
</p>

## Elona Extenderをインストール

Elona 1.22のカジノの描画バグをマシにしてくれたり、いろいろな便利機能を追加することができる[Elona Extender](https://www.elona-extender.com/k-tool/elona-extender/)をインストールします。
2017版ではなく最新版（2021）をインストールしましょう。

## まとめ

お疲れ様でした。
これでLinuxでもElonaを楽しむことができます。

各種ヴァリアントも通常通りインストールして遊ぶことができます（omake_overhaulやelm, URWは試したことがあります）。

MacでもWineが動けばいけると思います。
ただし2021/03/02現在、Catalina以降のOSでは32bitプログラムが未対応のため、WineはインストールできてもElonaは動かないようです。

それではよい冒険を！
